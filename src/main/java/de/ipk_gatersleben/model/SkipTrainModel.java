/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.model;

import java.util.List;

public class SkipTrainModel extends TrainModel{
	
	public SkipTrainModel(int featureSize, int window, double alpha, double sample, int iterations, int threadnumber)
	{
		super(featureSize, window, alpha, sample, iterations, threadnumber);
	}

	@Override
	/**
	 * skip gram model
	 * 
	 * @param curposition current word's position
	 * @param sentence a sentence sequence in the text
	 * @param contextlen the context length
	 */
	public void learnMethod(int curposition, List<WordNeuron> sentence, int contextlen) {
		WordNeuron word = sentence.get(curposition);
		int a, c = 0;
		for (a = contextlen; a < window * 2 + 1 - contextlen; a++) {
			if (a == window) {
				continue;
			}
			c = curposition - window + a;
			if (c < 0 || c >= sentence.size()) {
				continue;
			}

			double[] neu1e = new double[featureSize];
			// HIERARCHICAL SOFTMAX
			List<Neuron> neurons = word.getNeurons();
			WordNeuron we = sentence.get(c);
			for (int i = 0; i < neurons.size(); i++) {
				HiddenNeuron out = (HiddenNeuron) neurons.get(i);
				double f = 0;
				// Propagate hidden -> output
				for (int j = 0; j < featureSize; j++) {
					f += we.syn0[j] * out.syn1[j];
				}
				if (f <= -MAX_EXP || f >= MAX_EXP) {
					continue;
				} else {
					f = (f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2);
					f = expTable[(int) f];
				}
				// 'g' is the gradient multiplied by the learning rate
				double g = (1 - word.codeArr[i] - f) * alpha;
				// Propagate errors output -> hidden
				for (c = 0; c < featureSize; c++) {
					neu1e[c] += g * out.syn1[c];
				}
				// Learn weights hidden -> output
				for (c = 0; c < featureSize; c++) {
					out.syn1[c] += g * we.syn0[c];
				}
			}

			// Learn weights input -> hidden
			for (int j = 0; j < featureSize; j++) {
				we.syn0[j] += neu1e[j];
			}
		}

	}

}
