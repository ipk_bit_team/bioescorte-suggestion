/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.model;

import java.io.File;

import de.ipk_gatersleben.util.OptionsWord2Vec;
import de.ipk_gatersleben.util.Tools;
import uk.co.flamingpenguin.jewel.cli.ArgumentValidationException;
import uk.co.flamingpenguin.jewel.cli.CliFactory;

public class Word2Vec {

    private static void checkParameters(final OptionsWord2Vec options) {
	final String importpath = options.getCorpuspath();
	final String outputpath = options.getSaveModelpath();

	if (!Tools.checkFileexist(importpath)) {
	    System.err.println(
		    "Please check if the file \"" + importpath + "\" exists.");
	    System.exit(-1);
	}

	if (Tools.checkFileexist(outputpath)) {
	    System.err.println(
		    "The output file \"" + outputpath + "\" already exists.");
	    System.exit(-1);
	}

	try {
	    final File file = new File(outputpath);
	    final File directory = new File(
		    file.getParentFile().getAbsolutePath());
	    directory.mkdirs();
	    file.createNewFile();
	} catch (final Exception e) {
	    System.err.println("The output file \"" + outputpath
		    + "\" can't be created. Please check your file path or system permissions.");
	    System.exit(-1);
	}

	if (options.getFeaturesize() <= 0) {
	    System.err.println(
		    "Please specify a positive integer number, the value is feature size of word vectors.");
	    System.exit(-1);
	}

	if (options.getWindow() <= 1) {
	    System.err.println(
		    "Please specify a positive integer number, the value is the max skip length between words, it must be a value that more than 1.");
	    System.exit(-1);
	}

	if (options.getIterations() <= 0) {
	    System.err.println(
		    "Please specify a positive integer number, the value is the training iterations.");
	    System.exit(-1);
	}

	if (options.getAlpha() <= 0) {
	    System.err.println(
		    "Please specify a positive number, the value is the starting learning rate.");
	    System.exit(-1);
	}

	if (options.getSample() <= 0) {
	    System.err.println(
		    "Please specify a positive number, the value is the threshold for occurrence of words. Those phrases that appear with higher frequency in the training data will be randomly down-sampled.");
	    System.exit(-1);
	}

	if (options.getThreadnumber() <= 0) {
	    System.err.println(
		    "Please specify a positive integer number, the value is the training thread number.");
	    System.exit(-1);
	}

    }

    public static void main(final String[] args) throws Exception {
	System.err.println("LAILAPS-QSM: Word2Vec");
	OptionsWord2Vec options = null;
	try {
	    options = CliFactory.parseArguments(OptionsWord2Vec.class, args);
	} catch (final ArgumentValidationException e1) {
	    System.err.println(e1.getMessage());
	    System.exit(-1);
	}

	Word2Vec.checkParameters(options);

	TrainModel trainmodel = null;

	trainmodel = new SkipTrainModel(options.getFeaturesize(),
		options.getWindow(), options.getAlpha(), options.getSample(),
		options.getIterations(), options.getThreadnumber());

	trainmodel.learnVocabFromTrainFile(new File(options.getCorpuspath()));
	trainmodel.saveModel(new File(options.getSaveModelpath()));

    }

}
