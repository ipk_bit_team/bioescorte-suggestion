/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.model;

import java.util.List;

public class CbowTrainModel extends TrainModel{
	
	public CbowTrainModel(int featureSize, int window, double alpha, double sample, int iterations, int threadnumber)
	{
		super(featureSize, window, alpha, sample, iterations, threadnumber);
	}
	
	@Override
	/**
	 * cbow model
	 * 
	 * @param curposition current word's position
	 * @param sentence a sentence sequence in the text
	 * @param contextlen the context length
	 */
	public void learnMethod(int curposition, List<WordNeuron> sentence, int contextlen) {
		WordNeuron word = sentence.get(curposition);
		int a, c = 0;

		List<Neuron> neurons = word.getNeurons();
		double[] neu1e = new double[featureSize];
		double[] neu1 = new double[featureSize];
		WordNeuron last_word;

		for (a = contextlen; a < window * 2 + 1 - contextlen; a++)
			if (a != window) {
				c = curposition - window + a;
				if (c < 0)
					continue;
				if (c >= sentence.size())
					continue;
				last_word = sentence.get(c);
				if (last_word == null)
					continue;
				for (c = 0; c < featureSize; c++)
					neu1[c] += last_word.syn0[c];
			}

		// HIERARCHICAL SOFTMAX	
		for (int d = 0; d < neurons.size(); d++) {
			HiddenNeuron out = (HiddenNeuron) neurons.get(d);

			double f = 0;
			// Propagate hidden -> output
			for (c = 0; c < featureSize; c++)
				f += neu1[c] * out.syn1[c];
			if (f <= -MAX_EXP)
				continue;
			else if (f >= MAX_EXP)
				continue;
			else
				f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
			// 'g' is the error multiplied by the learning rate
			double g = f * (1 - f) * (word.codeArr[d] - f) * alpha;
			// Propagate errors output -> hidden
			for (c = 0; c < featureSize; c++) {
				neu1e[c] += g * out.syn1[c];
			}
			// Learn weights hidden -> output
			for (c = 0; c < featureSize; c++) {
				out.syn1[c] += g * neu1[c];
			}
		}
		for (a = contextlen; a < window * 2 + 1 - contextlen; a++) {
			if (a != window) {
				c = curposition - window + a;
				if (c < 0)
					continue;
				if (c >= sentence.size())
					continue;
				last_word = sentence.get(c);
				if (last_word == null)
					continue;
				for (c = 0; c < featureSize; c++)
					last_word.syn0[c] += neu1e[c];
			}

		}
	}
}
