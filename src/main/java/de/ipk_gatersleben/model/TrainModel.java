/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.model;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import de.ipk_gatersleben.util.Counter;
import de.ipk_gatersleben.util.StackTraceUtil;
import de.ipk_gatersleben.util.Tools;
import gnu.trove.iterator.TObjectIntIterator;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;


public abstract class TrainModel {
	private static final Logger logger = Logger	.getLogger(TrainModel.class);
	/**
	 * The feature size of word vectors
	 */
	protected int featureSize = 200;
	/**
	 * The max skip length between words
	 */
	protected int window = 5;
	/**
	 * Set threshold for occurrence of words. Those that appear with higher frequency in the training data will be randomly down-sampled
	 */
	private double sample = 1e-3;
	/**
	 * Set the starting learning rate
	 */
	protected double alpha = 0.025;
	protected int EXP_TABLE_SIZE = 1000;
	protected double[] expTable = new double[EXP_TABLE_SIZE];
	protected int MAX_EXP = 6;
	/**
	 * The training iterations
	 */
	private int iterations = 5;
	/**
	 * The training thread number
	 */
	private int threadnumber = 1;
	private long trainWordsCount = 0;
	private double startingAlpha = 0;
	private ProgressBar progressbar = null;
	private final Object updateLock = new Object();  
	volatile AtomicLong wordCount = new AtomicLong(0);
	long lastWordCount = 0;
	long wordCountActual = 0;
	long filelines = 0;	
	
	private Map<String, Neuron> wordMap = new HashMap<String, Neuron>();
	
	public TrainModel(int featureSize, int window, double alpha, double sample, int iterations, int threadnumber)
	{
		createExpTable();
		this.featureSize = featureSize;
		this.window = window;
		this.alpha = alpha;
		this.sample = sample;
		this.iterations = iterations;
		this.threadnumber = threadnumber;
		startingAlpha = alpha;
	}
	
	public void learnVocabFromTrainFile(File file) throws Exception
	{
		filelines = loadVocabandgetfileline(file);
		new HuffmanTree(featureSize).make(wordMap.values());
		for(int i=0;i<iterations;i++)
		{
			progressbar = new ProgressBar("Training iteration "+i, 100 , ProgressBarStyle.ASCII);
			progressbar.start();
			splitTask(file);
			progressbar.stop();
		}
	}
	
	private void splitTask(File file) throws IOException
	{		
		long singleline = filelines / threadnumber;
		if(singleline * threadnumber < filelines)
		{
			singleline++;
		}
		
		ExecutorService trainexecutorService = Executors.newFixedThreadPool(threadnumber);
		List<Future<String>> resultList = new ArrayList<Future<String>>();
		try
		{
			for(long i=0;i<threadnumber;i++)
			{
				Future<String> future = trainexecutorService.submit(new TrainWorker(file,i*singleline,(i+1)*singleline,i));
				resultList.add(future);
			}
			
			for (Future<String> fs : resultList) { 
	            try { 
	            	fs.get();         
	            } catch (InterruptedException e) { 
	            	logger.error(StackTraceUtil.getStackTrace(e));
	            } catch (ExecutionException e) { 
	            	logger.error(StackTraceUtil.getStackTrace(e)); 
	            } 
			} 
		}
		catch(Exception e)
		{
			logger.error(StackTraceUtil.getStackTrace(e));
			System.err.println("can't train the model, for more detailed information please refer to " + System.getProperty("user.dir") + File.separator + "lailaps.log.");
			System.exit(-1);
		}
		finally
		{
			trainexecutorService.shutdown(); 
		}
	}
	
	/**
     * Save the model to File
     *
     * @param file the File which will be used to save model
     */ 
	public void saveModel(File file) {
		try {
			 
			DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			dataOutputStream.writeInt(wordMap.size());
			dataOutputStream.writeInt(featureSize);
			
			double[] syn0 = null;
			for (Entry<String, Neuron> element : wordMap.entrySet()) {
				dataOutputStream.writeUTF(element.getKey());
				
				syn0 = ((WordNeuron) element.getValue()).syn0;
				//normalization to speed up the Cosine similarity computation in the online step
				double len = 0;
				for (double d : syn0) {
					len += d * d;
				}
				
				len = Math.sqrt(len);
				
				for (double d : syn0) {
					dataOutputStream.writeFloat(((Double) (d/len)).floatValue());
				}
			}
			dataOutputStream.close();
			
		} catch (IOException e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}
	}
	
	public abstract void learnMethod(int index, List<WordNeuron> sentence, int b);
	
	/**
	 * Precompute the exp() 
	   Precompute f(x) = x / (x + 1)
	 */
	private void createExpTable() {
		for (int i = 0; i < EXP_TABLE_SIZE; i++) {
			expTable[i] = Math
					.exp(((i / (double) EXP_TABLE_SIZE * 2 - 1) * MAX_EXP));
			expTable[i] = expTable[i] / (expTable[i] + 1);
		}
	}
	
	/**
	 * compute the frequency
	 * 
	 * @param file
	 * @throws IOException
	 */
	private long loadVocabandgetfileline(File file) throws IOException {
		Counter<String> mc = new Counter<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		long filelines = 0;
		String temp = null;
		while ((temp = br.readLine()) != null) {
			String[] split = Tools.split(temp," ");
			trainWordsCount += split.length;
			for (String string : split) {
				mc.add(string);
			}
			filelines++;
		}
		
		logger.info("trainWordsCount: " + trainWordsCount);
		br.close();

		for(TObjectIntIterator<String> element = mc.get().iterator(); element.hasNext();)
		{
			element.advance();
			wordMap.put(element.key(), new WordNeuron(element.key(),element.value(), featureSize));
		}
		logger.info("wordMap size: " + wordMap.size());
		
		return filelines;
	}
	
	class TrainWorker implements Callable<String>
	{
		private File file;
		private long startline;
		private long endline;
		private long currentfileline = 0;
		private long nextRandom;
				
		public TrainWorker(File file, long startline, long endline, long idx)
		{
			this.file = file;
			this.startline = startline;
			this.endline = endline;
			this.nextRandom = idx;
		}
		
		public String call()  {
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				String temp = null;
				while ((temp = br.readLine()) != null) {
					currentfileline++;
					if(currentfileline > startline && currentfileline <= endline)
					{
						long currentcount = wordCount.get();
						if (currentcount - lastWordCount > 10000) {
							progressbar.setExtraMessage("alpha:" + alpha);
							progressbar.stepTo((long) (wordCountActual	/ (double) (trainWordsCount + 1) * 100));
							
							 synchronized (updateLock){
								 wordCountActual += currentcount - lastWordCount;
								 lastWordCount = currentcount;
								 alpha = startingAlpha
											* (1 - wordCountActual / (double) (trainWordsCount + 1));
								if (alpha < startingAlpha * 0.0001) {
									alpha = startingAlpha * 0.0001;
								}
							 }							
						}
						
						String[] strs = Tools.split(temp," ");
						wordCount.addAndGet(strs.length);
						List<WordNeuron> sentence = new ArrayList<WordNeuron>();
						for (int i = 0; i < strs.length; i++) {
							Neuron entry = wordMap.get(strs[i]);
							if (entry == null) {
								continue;
							}
							// The subsampling randomly discards frequent words while keeping the ranking same
							if (sample > 0) {
								double ran = (Math.sqrt(entry.freq
										/ (sample * trainWordsCount)) + 1)
										* (sample * trainWordsCount) / entry.freq;
								nextRandom = nextRandom * 25214903917L + 11;
								if (ran < (nextRandom & 0xFFFF) / (double) 65536) {
									continue;
								}
								sentence.add((WordNeuron) entry);
							}
						}

						for (int index = 0; index < sentence.size(); index++) {
							nextRandom = nextRandom * 25214903917L + 11;				
							learnMethod(index, sentence, (int) nextRandom % window);				
						}
					}
					else if(currentfileline > endline)
					{
						break;
					}
				}
			}
			catch (Exception ex)
			{
				logger.fatal(StackTraceUtil.getStackTrace(ex));
			}
			finally
			{
				if(br!=null)
				{
					try
					{
						br.close();
					}
					catch (Exception ex)
					{
						//ignore
					}
				}				
			}
			return null;
		}
	}
}
