/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.model;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import de.ipk_gatersleben.util.Counter;
import de.ipk_gatersleben.util.OptionsWord2Phrase;
import de.ipk_gatersleben.util.Tools;
import gnu.trove.iterator.TObjectIntIterator;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;
import uk.co.flamingpenguin.jewel.cli.ArgumentValidationException;
import uk.co.flamingpenguin.jewel.cli.CliFactory;

public class Word2Phrase {
    private static Counter<String> mc = new Counter<String>();
    private static long trainwordscount = 0L;
    private static final int BATCHSIZE = 10000000;
    private static int min_count = 5;
    private static double threshold = 100d;

    private static void checkParameters(final OptionsWord2Phrase options) {
	final String importpath = options.getCorpuspath();
	final String outputpath = options.getPhrasepath();

	if (!Tools.checkFileexist(importpath)) {
	    System.err.println(
		    "Please check if the file \"" + importpath + "\" exists.");
	    System.exit(-1);
	}

	if (Tools.checkFileexist(outputpath)) {
	    System.err.println(
		    "The output file \"" + outputpath + "\" already exists.");
	    System.exit(-1);
	}

	try {
	    final File file = new File(outputpath);
	    final File directory = new File(
		    file.getParentFile().getAbsolutePath());
	    directory.mkdirs();
	    file.createNewFile();
	} catch (final Exception e) {
	    System.err.println("The output file \"" + outputpath
		    + "\" can't be created. Please check your file path or system permissions.");
	    System.exit(-1);
	}

	if (options.getMincount() <= 0) {
	    System.err.println(
		    "Please specify a positive integer number, the words that appear less than this number will be discarded.");
	    System.exit(-1);
	}

	if (options.getThreshold() <= 0) {
	    System.err.println(
		    "Please specify a positive number, the value represents threshold for forming the phrases (higher means less phrases, 100 is a suitable value for a billion scale dataset).");
	    System.exit(-1);
	}
    }

    private static long estimateFilelines(final String path) {
	try {
	    return Tools.estimateFilelines(path);
	} catch (final Exception e) {
	    System.err.println("can't parse text file:" + path + ".");
	    System.exit(-1);
	}
	return 0L;
    }

    /**
     * @param args
     */
    public static void main(final String[] args) throws IOException {
	System.err.println("LAILAPS-QSM: Word2Phrase");
	OptionsWord2Phrase options = null;
	try {
	    options = CliFactory.parseArguments(OptionsWord2Phrase.class, args);
	} catch (final ArgumentValidationException e1) {
	    System.err.println(e1.getMessage());
	    System.exit(-1);
	}

	Word2Phrase.min_count = options.getMincount();
	Word2Phrase.threshold = options.getThreshold();

	Word2Phrase.checkParameters(options);

	final PrintWriter writer = new PrintWriter(new BufferedOutputStream(
		new FileOutputStream(options.getPhrasepath())));

	final ProgressBar progressbar = new ProgressBar("Create Phrases",
		Word2Phrase.estimateFilelines(options.getCorpuspath()) * 2,
		ProgressBarStyle.ASCII);
	progressbar.start();

	final Word2Phrase word2phrase = new Word2Phrase();
	final String corpuspath = options.getCorpuspath();
	word2phrase.loadvocabfromtrain(corpuspath, Word2Phrase.mc, progressbar);
	word2phrase.filtervocab(Word2Phrase.mc);

	BufferedReader br = null;
	Reader fr = null;
	fr = new FileReader(corpuspath);
	br = new BufferedReader(fr);
	String myreadline;

	progressbar.setExtraMessage(
		"writing phrases to " + options.getPhrasepath() + ".");

	while (br.ready()) {
	    myreadline = br.readLine();

	    progressbar.step();

	    final PairWise[] pairwises = word2phrase.getPairwise(myreadline);
	    final StringBuilder sb = new StringBuilder();
	    for (final PairWise pairwise : pairwises) {
		final Integer pa = Word2Phrase.mc.get()
			.get(pairwise.getFirst());
		Integer pb = null;
		Integer pab = null;
		if (pairwise.getSecond() != null) {
		    pb = Word2Phrase.mc.get().get(pairwise.getSecond());
		    pab = Word2Phrase.mc.get().get(
			    pairwise.getFirst() + "_" + pairwise.getSecond());
		}
		double score = 0d;
		if (pa != null && pb != null && pab != null) {
		    score = (pab.doubleValue() - Word2Phrase.min_count)
			    / pa.doubleValue() / pb.doubleValue()
			    * Word2Phrase.trainwordscount;
		}

		if (score > Word2Phrase.threshold) {
		    sb.append(pairwise.getFirst() + "_" + pairwise.getSecond());
		    sb.append(" ");
		} else {
		    if (pa != null && pa.intValue() >= Word2Phrase.min_count) {
			sb.append(pairwise.getFirst());
			sb.append(" ");
		    }
		}
	    }
	    writer.append(sb.toString().trim());
	    writer.println();
	}
	br.close();
	fr.close();

	writer.flush();
	writer.close();

	progressbar.stop();
    }

    // Reduces the vocabulary by removing infrequent tokens
    private void filtervocab(final Counter<String> mc) {
	final TObjectIntIterator<String> it = mc.get().iterator();
	while (it.hasNext()) {
	    it.advance();
	    final int value = it.value();
	    if (value < Word2Phrase.min_count) {
		it.remove();
	    }
	}
    }

    private PairWise[] getPairwise(final String line) {
	final String splitarray[] = Tools.split(line.trim(), " ");
	final List<PairWise> list = new ArrayList<PairWise>();

	if (splitarray.length > 0) {
	    if (splitarray.length == 1) {
		list.add(new PairWise(splitarray[0], null));
	    }
	    for (int i = 1; i < splitarray.length; i++) {
		if (i == splitarray.length - 1) {
		    list.add(new PairWise(splitarray[i], null));
		} else {
		    final int ipos1 = splitarray[i - 1].indexOf(":");
		    final int ipos2 = splitarray[i].indexOf(":");
		    if (ipos1 >= 0 || ipos2 >= 0) {
			list.add(new PairWise(splitarray[i - 1], null));
		    } else {
			list.add(
				new PairWise(splitarray[i - 1], splitarray[i]));
		    }
		}
	    }
	}

	return list.toArray(new PairWise[0]);
    }

    // Load the words to the vocabulary
    private void loadvocabfromtrain(final String path, final Counter<String> mc,
	    final ProgressBar progressbar) throws IOException {
	final BufferedReader br = new BufferedReader(
		new InputStreamReader(new FileInputStream(path)));

	String temp = null;
	while ((temp = br.readLine()) != null) {
	    progressbar.step();
	    final PairWise[] pairwises = this.getPairwise(temp);
	    for (final PairWise pairwise : pairwises) {
		mc.add(pairwise.getFirst());
		if (pairwise.getSecond() != null) {
		    mc.add(pairwise.getFirst() + "_" + pairwise.getSecond());
		}
		Word2Phrase.trainwordscount++;
		if (Word2Phrase.trainwordscount % Word2Phrase.BATCHSIZE == 0) {
		    progressbar.setExtraMessage(
			    "reading " + Word2Phrase.trainwordscount
				    + " words from " + path + ".");
		}
	    }
	}

	br.close();
	progressbar.setExtraMessage(
		"the total number of words is:" + Word2Phrase.trainwordscount);
    }
}
