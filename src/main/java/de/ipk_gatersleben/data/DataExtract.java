/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.data;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import de.ipk_gatersleben.model.PairWise;
import de.ipk_gatersleben.util.OptionsDataExtract;
import de.ipk_gatersleben.util.StackTraceUtil;
import de.ipk_gatersleben.util.TextContent;
import de.ipk_gatersleben.util.Tools;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;
import uk.co.flamingpenguin.jewel.cli.ArgumentValidationException;
import uk.co.flamingpenguin.jewel.cli.CliFactory;

public class DataExtract {
    private static final Logger logger = Logger.getLogger(DataExtract.class);
    private static String importpath;
    private static String outputfilepath;
    private static int fileformat;
    private static Set<String> stopwords = new HashSet<String>();

    private static void checkParameters(final OptionsDataExtract options) {
	DataExtract.importpath = options.getCorpuspath();
	DataExtract.outputfilepath = options.getOutputpath();
	DataExtract.fileformat = options.getFormat();

	if (!Tools.checkFolderexist(DataExtract.importpath)) {
	    System.err.println("Please check if the folder \""
		    + DataExtract.importpath + "\" exists.");
	    System.exit(-1);
	}

	if (!Tools.checkFolderexist(DataExtract.outputfilepath)) {
	    System.err.println("Please check if the folder \""
		    + DataExtract.outputfilepath + "\" exists.");
	    System.exit(-1);
	}
	if (DataExtract.fileformat == 0 || DataExtract.fileformat == 1) {
	    // remove some code
	} else {
	    System.err.println(
		    "Please specify the format of input file, 0 is PubMed text format file, 1 is others format(each line a document)!");
	    System.exit(-1);
	}
    }

    private static List<String> generateTokens(final String content)
	    throws Exception {
	final List<String> tokens = new ArrayList<String>(200);
	final Analyzer myAnalyzer = new StandardAnalyzer();
	try {
	    final java.io.Reader Strreader = new StringReader(content);
	    final TokenStream ts = myAnalyzer.tokenStream("content", Strreader);
	    ts.reset();
	    ts.addAttribute(CharTermAttribute.class);
	    while (ts.incrementToken()) {
		final CharTermAttribute ta = ts
			.getAttribute(CharTermAttribute.class);
		if (!DataExtract.stopwords.contains(ta.toString())) {
		    tokens.add(ta.toString());
		}
	    }
	} catch (final Exception e) {
	    throw e;
	} finally {
	    myAnalyzer.close();
	}

	return tokens;
    }

    private static PairWise handledata(final List<String> list,
	    final int documentid) {
	final String title = list.get(0).indexOf(documentid + ".") == 0
		? list.get(1) : list.get(0);
	final List<TextContent> sublist = new ArrayList<TextContent>();
	for (int i = 1; i < list.size(); i++) {
	    if (list.get(i).indexOf("Author information") < 0) {
		final TextContent obj = new TextContent(i,
			list.get(i).length());
		sublist.add(obj);
	    }
	}
	Collections.sort(sublist);
	final String content = list
		.get(sublist.get(sublist.size() - 1).getIdx());
	return new PairWise(title, content);
    }

    /**
     * @param args
     */
    public static void main(final String[] args) throws Exception {
	System.err.println("LAILAPS-QSM: DataExtract");
	OptionsDataExtract options = null;
	try {
	    options = CliFactory.parseArguments(OptionsDataExtract.class, args);
	} catch (final ArgumentValidationException e1) {
	    System.err.println(e1.getMessage());
	    System.exit(-1);
	}
	DataExtract.importpath = options.getCorpuspath();
	DataExtract.outputfilepath = options.getOutputpath();
	DataExtract.fileformat = options.getFormat();

	DataExtract.checkParameters(options);

	try {
	    DataExtract.stopwords = Tools.initstopword();
	} catch (final Exception e) {
	    System.err.println("can't load stop words!");
	    System.exit(-1);
	}

	final PrintWriter writer = new PrintWriter(new BufferedOutputStream(
		new FileOutputStream(DataExtract.outputfilepath + File.separator
			+ "corpus.txt")));

	final File dir = new File(DataExtract.importpath);

	long filelines = 0L;
	final List<Long> lineslist = new ArrayList<Long>();

	for (final File file : dir.listFiles()) {
	    if (file.isDirectory()) {
		// only read the Files under folder "importpath"
		continue;
	    }
	    final long fileline = Tools
		    .estimateFilelines(file.getAbsolutePath());
	    filelines += fileline;
	    lineslist.add(fileline);
	}

	final ProgressBar progressbar = new ProgressBar("Parse files",
		filelines, ProgressBarStyle.ASCII);

	progressbar.start();

	long currentline = 0;
	for (final File file : dir.listFiles()) {
	    if (file.isDirectory()) {
		// only read the Files under folder "importpath"
		continue;
	    }

	    progressbar.setExtraMessage("Read file " + file.getName());

	    if (DataExtract.fileformat == 1) {
		String temp = null;
		final BufferedReader br = new BufferedReader(
			new InputStreamReader(new FileInputStream(file)));
		try {
		    while ((temp = br.readLine()) != null) {
			currentline++;
			progressbar.stepTo(currentline);
			List<String> tokens = null;
			try {
			    tokens = DataExtract.generateTokens(temp);
			} catch (final Exception e) {
			    DataExtract.logger
				    .error(StackTraceUtil.getStackTrace(e));
			}
			if (tokens.size() == 0) {
			    continue;
			}
			for (int i = 0; i < tokens.size() - 1; i++) {
			    writer.print(tokens.get(i) + " ");
			}
			writer.print(tokens.get(tokens.size() - 1));
			writer.println();
		    }
		} catch (final Exception e) {
		    DataExtract.logger.error(StackTraceUtil.getStackTrace(e));
		    System.err.println("can't parse text file:" + file.getName()
			    + ", for more detailed information please refer to "
			    + System.getProperty("user.dir") + File.separator
			    + "lailaps.log.");
		    System.exit(-1);
		} finally {
		    br.close();
		}

	    } else {
		String temp = null;
		final List<String> list = new ArrayList<String>();
		final StringBuilder sb = new StringBuilder();
		final BufferedReader br = new BufferedReader(
			new InputStreamReader(new FileInputStream(file)));
		int curlinelen = -1;
		int documentid = 1;
		try {
		    while ((temp = br.readLine()) != null) {
			currentline++;
			progressbar.stepTo(currentline);
			curlinelen = temp.length();
			if (curlinelen == 0 && sb.length() > 0) {
			    list.add(sb.toString());
			    sb.setLength(0);
			} else if (curlinelen > 0) {
			    if (temp.startsWith(documentid + 1 + ". ")) {
				// next document
				final PairWise pairwise = DataExtract
					.handledata(list, documentid);
				final List<String> tokens = DataExtract
					.generateTokens(pairwise.getFirst()
						+ " " + pairwise.getSecond());
				if (tokens.size() == 0) {
				    continue;
				}
				for (int i = 0; i < tokens.size() - 1; i++) {
				    writer.print(tokens.get(i) + " ");
				}
				writer.print(tokens.get(tokens.size() - 1));
				writer.println();
				list.clear();

				documentid++;
			    }

			    sb.append(temp + " ");
			}
		    }
		} catch (final Exception e) {
		    DataExtract.logger.error(StackTraceUtil.getStackTrace(e));
		    System.err.println("can't parse text file:" + file.getName()
			    + ", for more detailed information please refer to "
			    + System.getProperty("user.dir") + File.separator
			    + "lailaps.log.");
		    System.exit(-1);
		} finally {
		    br.close();
		}

	    }
	}

	writer.flush();
	writer.close();

	progressbar.stop();
    }

}
