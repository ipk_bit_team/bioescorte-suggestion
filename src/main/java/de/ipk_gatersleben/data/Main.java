package de.ipk_gatersleben.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import de.ipk_gatersleben.model.Word2Phrase;
import de.ipk_gatersleben.model.Word2Vec;
import de.ipk_gatersleben.util.OptionMain;
import de.ipk_gatersleben.util.StackTraceUtil;
import uk.co.flamingpenguin.jewel.cli.ArgumentValidationException;
import uk.co.flamingpenguin.jewel.cli.CliFactory;

/**
 * 
 * this class is a wrapper to DataExtract, Word2Phrase and Word2Vec
 *
 */
public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    private static String importpath;
    private static String outputfilepath;
    private static int fileformat;

    private static void cleanup(final String foldername) {
	final File tempfile = new File(
		foldername + File.separator + "corpus.txt");
	if (tempfile.exists()) {
	    tempfile.delete();
	}
	/*
	 * tempfile = new File(foldername + File.separator + "phrase.txt");
	 * if(tempfile.exists()) { tempfile.delete(); }
	 */
    }

    public static void main(final String args[]) {
	System.err.println("LAILAPS-QSM: Query Suggestion Model Build Suite");
	OptionMain options = null;
	try {
	    options = CliFactory.parseArguments(OptionMain.class, args);
	} catch (final ArgumentValidationException e1) {
	    System.err.println(e1.getMessage());
	    System.exit(-1);
	}

	Main.importpath = options.getCorpuspath();
	Main.outputfilepath = options.getOutputpath();
	Main.fileformat = options.getFormat();

	final long t1 = System.currentTimeMillis();

	try {
	    final List<String> formatfilearrlist = new ArrayList<String>();

	    formatfilearrlist.add("-i");
	    formatfilearrlist.add(Main.importpath);

	    formatfilearrlist.add("-o");
	    formatfilearrlist.add(Main.outputfilepath);

	    formatfilearrlist.add("-f");
	    formatfilearrlist.add(Integer.toString(Main.fileformat));

	    DataExtract.main(formatfilearrlist.toArray(new String[0]));
	} catch (final Exception e) {
	    Main.logger.error(StackTraceUtil.getStackTrace(e));
	    System.err.println("can't format inputfile at: " + Main.importpath);
	    Main.cleanup(Main.outputfilepath);
	    System.exit(-1);
	}

	try {
	    final List<String> word2phrasearrlist = new ArrayList<String>();

	    word2phrasearrlist.add("-i");
	    word2phrasearrlist
		    .add(Main.outputfilepath + File.separator + "corpus.txt");

	    word2phrasearrlist.add("-o");
	    word2phrasearrlist
		    .add(Main.outputfilepath + File.separator + "phrase.txt");

	    word2phrasearrlist.add("-m");
	    word2phrasearrlist.add("5");

	    word2phrasearrlist.add("-t");
	    word2phrasearrlist.add("100");

	    Word2Phrase.main(word2phrasearrlist.toArray(new String[0]));
	} catch (final Exception e) {
	    Main.logger.error(StackTraceUtil.getStackTrace(e));
	    System.err.println("can't format inputfile at: "
		    + Main.outputfilepath + File.separator + "corpus.txt");
	    Main.cleanup(Main.outputfilepath);
	    System.exit(-1);
	}

	try {
	    final List<String> word2vecarrlist = new ArrayList<String>();

	    word2vecarrlist.add("-i");
	    word2vecarrlist
		    .add(Main.outputfilepath + File.separator + "phrase.txt");

	    word2vecarrlist.add("-o");
	    word2vecarrlist
		    .add(Main.outputfilepath + File.separator + "model.bin");

	    word2vecarrlist.add("-f");
	    word2vecarrlist.add(Integer.toString(options.getFeaturesize()));

	    word2vecarrlist.add("-w");
	    word2vecarrlist.add(Integer.toString(options.getWindow()));

	    word2vecarrlist.add("-s");
	    word2vecarrlist.add("0.001");

	    word2vecarrlist.add("-t");
	    word2vecarrlist.add(Integer.toString(options.getIterations()));

	    word2vecarrlist.add("-a");
	    word2vecarrlist.add(Double.toString(options.getAlpha()));

	    word2vecarrlist.add("-n");
	    word2vecarrlist.add(Integer.toString(options.getThreadnumber()));

	    Word2Vec.main(word2vecarrlist.toArray(new String[0]));
	} catch (final Exception e) {
	    Main.logger.error(StackTraceUtil.getStackTrace(e));
	    Main.cleanup(Main.outputfilepath);
	    System.exit(-1);
	}

	long t2 = System.currentTimeMillis() - t1;
	t2 = t2 / (1000 * 60);
	Main.printmessage(options, t2);

	Main.cleanup(Main.outputfilepath);
    }

    private static void printmessage(final OptionMain options,
	    final long mitnues) {
	final String message = "training finished successfully in " + mitnues
		+ " minutes. input: the phrases corpus from "
		+ options.getCorpuspath() + " output: "
		+ options.getOutputpath() + ". \r\n ";
	System.out.println(message);
    }
}
