/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.ipk_gatersleben.util.CorpusContainer;
import de.ipk_gatersleben.util.StackTraceUtil;
import de.ipk_gatersleben.util.Config;

public class SemanticsuggestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(SemanticsuggestionServlet.class);
	private static final String URLPATTERN = "/";

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			logger.info("data loading");
			CorpusContainer.loadModel(Config.getCorpuspath());
			logger.info("data load finish");
		} catch (Exception e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}
	}

	
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String query = request.getPathInfo();		
		logger.info("IP####:" + getIP(request) + " #### request post query###:"
				+ query);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
		obj.put("errorinfo", "The method post is not allowed for this resource.");
		PrintWriter out = response.getWriter();
		out.write(obj.toJSONString());
	}

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String pathinfo = request.getPathInfo();
		logger.info("IP####:" + getIP(request) + " #### request get query###:"
				+ pathinfo);
		int iposstart = pathinfo.indexOf(URLPATTERN);
		
		org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		
		if(iposstart>=0 && pathinfo.length()>(iposstart+URLPATTERN.length()))
		{
			String query = pathinfo.substring(iposstart+URLPATTERN.length());
			List<String> suggestions = CorpusContainer.getSuggestsemantic(query,getIP(request));						
			obj.put("suggestions", suggestions);
		}
		else
		{
			obj.put("errorinfo", "please set correct query parameter");
		}
		
		PrintWriter out = response.getWriter();
		out.write(obj.toJSONString());
	}

	private String getIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
