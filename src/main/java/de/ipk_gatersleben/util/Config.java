/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import de.ipk_gatersleben.util.StackTraceUtil;


public class Config {
	private static final Logger logger = Logger.getLogger(Config.class);
	private static final Properties properties = new Properties();
	static {
		try {
			properties.load(Config.class
					.getResourceAsStream("/application.properties"));
		} catch (IOException e) {
		
		}
	}
	
	public static String getCorpuspath()
    {
    	return properties.get("corpus.location").toString();
    }
	
	public static void setCorpuspath(String corpuspath)
	{
		properties.put("corpus.location", corpuspath);
		try {
			String path = Config.class.getClass().getResource("/").getPath();
		    path=path.substring(1, path.indexOf("classes"));
		    
			FileOutputStream fos = new FileOutputStream(path+"application.properties");
			properties.store(fos, "set corpus path");
			fos.flush();

			fos.close();
		}
		catch (IOException e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}
	}
}
