/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import uk.co.flamingpenguin.jewel.cli.CommandLineInterface;
import uk.co.flamingpenguin.jewel.cli.Option;

@CommandLineInterface(application = "dataextract")
public interface OptionsDataExtract {
	@Option(shortName = "i", description = "Please specify the folder path to input files!")
	String getCorpuspath();
	@Option(shortName = "o", description = "Please specify the folder path to out file(this program will generate a corpus file in output folder, it's name is corpus.txt)!")
	String getOutputpath();
	@Option(shortName = "f", description = "Please specify the format of input file, 0 is PubMed text format file, 1 is others format(each line a document)!")
	int getFormat();
}
