/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import de.ipk_gatersleben.util.WordEntry;



public class CorpusContainer {
	private static final Logger logger = Logger.getLogger(CorpusContainer.class);
	private static int topNSize = 15;
	private static int words;
	private static int size;
	private static HashMap<String, float[]> wordMap = new HashMap<String, float[]>();
	
	public static void loadModel(String path) throws Exception {
		DataInputStream dis = new DataInputStream(new BufferedInputStream(
				new FileInputStream(path)));
		words = dis.readInt();
		size = dis.readInt();
		float vector = 0;
		int corpussize = 0;
		
		String key = null;
		float[] value = null;
	
		for (int i = 0; i < words; i++) {
			key = dis.readUTF();
			value = new float[size];
			for (int j = 0; j < size; j++) {
				vector = dis.readFloat();
				value[j] = vector;
			}

			wordMap.put(key.replace('_', ' '), value);
			corpussize++;
		}
		dis.close();
		logger.info("corpus size:" + corpussize);
	}	
	
	
	public static List<String> getSuggestsemantic(String query, String ipaddress)
	{
		return getSuggestions(query);
	}
	
	private static List<String> getSuggestions(String query)
	{
		long t1 = System.currentTimeMillis();
		Analyzer myAnalyzer = new StandardAnalyzer();
		List<String> tokenlist = new ArrayList<String>();	
		try {
			TokenStream ts = myAnalyzer.tokenStream("test", query);
			ts.reset();
			ts.addAttribute(CharTermAttribute.class);
			while (ts.incrementToken()) {
				CharTermAttribute ta = ts.getAttribute(CharTermAttribute.class);
				tokenlist.add(ta.toString());
			}
		} catch (IOException e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}

		myAnalyzer.close();
	
    	String[] arr = tokenlist.toArray(new String[0]);
    	List<String> querylist = new ArrayList<String>();
    	List<String> candidatesuggestionlist = new ArrayList<String>();
    	if(arr.length>2)
    	{
    		for(int i=0;i<arr.length-1;i++)
    		{
    			querylist.add(arr[i]+" "+arr[i+1]);
    			Set<WordEntry> suggestions = distance(arr[i]+" "+arr[i+1]);
    			if(suggestions!=null)
    			{
    				StringBuilder presb = new StringBuilder();
    				for(int j=0;j<i;j++)
    				{
    					presb.append(arr[j]);
    					presb.append(" ");
    				}
    				StringBuilder postsb = new StringBuilder();
    				for(int j=(i+2);j<arr.length;j++)
    				{
    					postsb.append(arr[j]);
    					postsb.append(" ");
    				}
        			for(WordEntry word:suggestions)
        			{
        				candidatesuggestionlist.add((presb.toString() + word.name + " " + postsb.toString()).trim());
        			}
    			}
    		}
    	}
    	else if(arr.length>0)
    	{
    		Set<WordEntry> suggestions = distance(arr.length>1 ? arr[0]+" "+arr[1] : arr[0]);
    		if(suggestions!=null)
    		{
    			for(WordEntry word:suggestions)
    			{
    				candidatesuggestionlist.add(word.name);
    				System.out.println(word.name + "->" + word.similarity);
    			}
    		}
    	}
    	long t2 = System.currentTimeMillis()-t1;
    	logger.info("time is:"+(t2/1000));
    	return candidatesuggestionlist;
	}
	
	private static Set<WordEntry> distance(String word) {
		float[] wordVector = getWordVector(word);
		if (wordVector == null) {
			return null;
		}
		
		Set<Entry<String, float[]>> entrySet = wordMap.entrySet();
		TopN<WordEntry> topEntry = new TopN<>(topNSize);
		String name = null;
		for (Entry<String, float[]> entry : entrySet) {
			name = entry.getKey();
			if (name.equals(word)) {
				continue;
			}
			double dist = cosineSimilarity(entry.getValue(),wordVector);
			topEntry.add(new WordEntry(name,(float)dist));
		}
		List<WordEntry> wordEntrys = topEntry.toList(); 
		return new TreeSet<WordEntry>(wordEntrys);
	}
	
	private static double cosineSimilarity(float[] vectorA, float[] vectorB) {
		/*
	    double dotProduct = 0.0;
	    double normA = 0.0;
	    double normB = 0.0;
	    for (int i = 0; i < vectorA.length; i++) {
	        dotProduct += vectorA[i] * vectorB[i];
	        normA += Math.pow(vectorA[i], 2);
	        normB += Math.pow(vectorB[i], 2);
	    }   
	    return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	    */
		//after the normalization, we compute only dotproduct
		double dotProduct = 0.0;
	    for (int i = 0; i < vectorA.length; i++) {
	        dotProduct += vectorA[i] * vectorB[i];
	    }   
	    return dotProduct;
	}
	
	private static float[] getWordVector(String word) {
		return wordMap.get(word);
	}
	
}
