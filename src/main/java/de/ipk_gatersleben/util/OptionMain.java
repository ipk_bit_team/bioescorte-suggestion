package de.ipk_gatersleben.util;

import uk.co.flamingpenguin.jewel.cli.Option;

public interface OptionMain {
	@Option(shortName = "i", description = "Please specify the folder path to input files!")
	String getCorpuspath();
	@Option(shortName = "o", description = "Please specify the folder path to out file(this program will generate a training model file in output folder, it's name is model.bin)!")
	String getOutputpath();
	@Option(shortName = "m", defaultValue = "200", description = "The feature size of word vectors.")
	int getFeaturesize();
	@Option(shortName = "w", defaultValue = "5", description = "The max skip length between words.")
	int getWindow();
	@Option(shortName = "t", defaultValue = "1", description = "The training iterations (default 1).")
	int getIterations();
	@Option(shortName = "a", defaultValue = "0.025", description = "Set the starting learning rate; default is 0.025.")
	double getAlpha();
	@Option(shortName = "n", defaultValue = "1", description = "The training thread number, default is 1.")
	int getThreadnumber();
	@Option(shortName = "f", description = "Please specify the format of input file, 0 is PubMed text format file, 1 is others format(each line a document)!")
	int getFormat();
	@Option(shortName = "a", defaultValue = "1", description = "Use the continuous bag of words model; default is 1 (use 0 for skip-gram model).")
	int getMethod();
}
