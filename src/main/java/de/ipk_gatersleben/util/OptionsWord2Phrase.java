/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import uk.co.flamingpenguin.jewel.cli.CommandLineInterface;
import uk.co.flamingpenguin.jewel.cli.Option;

@CommandLineInterface(application = "word2phrase")
public interface OptionsWord2Phrase {
	@Option(shortName = "i", description = "Please specify the path to corpus!")
	String getCorpuspath();
	@Option(shortName = "o", description = "Please specify the path to phrase!")
	String getPhrasepath();
	@Option(shortName = "m", defaultValue = "5", description = "The words that appear less than this number will be discarded.")
	int getMincount();
	@Option(shortName = "t", defaultValue = "100", description = "The value represents threshold for forming the phrases (higher means less phrases, 100 is a suitable value for a billion scale dataset).")
	double getThreshold();
}
