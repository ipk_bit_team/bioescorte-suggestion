/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

public class Tools {
    public static boolean checkFileexist(final String filepath) {
	try {
	    final File file = new File(filepath);
	    if (file.exists()) {
		return true;
	    }
	} catch (final Exception e) {
	    return false;
	}
	return false;
    }

    public static boolean checkFolderexist(final String filepath) {
	try {
	    final File file = new File(filepath);
	    if (file.exists()) {
		return file.isDirectory();
	    }
	} catch (final Exception e) {
	    return false;
	}
	return false;
    }

    public static long estimateFilelines(final String path) throws Exception {
	BufferedReader br = null;
	long filelines = 0L;
	try {
	    br = new BufferedReader(
		    new InputStreamReader(new FileInputStream(path)));
	    while ((br.readLine()) != null) {
		filelines++;
	    }
	} catch (final Exception e) {
	    throw e;
	} finally {
	    try {
		br.close();
	    } catch (final Exception e) {
		// ignore
	    }
	}

	return filelines;
    }

    public static Set<String> initstopword() throws Exception {
	BufferedReader br = null;
	Reader fr = null;

	final Set<String> stopwords = new HashSet<String>();

	fr = new InputStreamReader(
		Tools.class.getResourceAsStream("/stopwords.txt"), "UTF-8");
	br = new BufferedReader(fr);

	String myreadline;
	while (br.ready()) {
	    myreadline = br.readLine();
	    if (myreadline.trim().length() > 0) {
		stopwords.add(myreadline.trim().toLowerCase());
	    }
	}
	br.close();
	fr.close();

	return stopwords;
    }

    // a faster split method compare with jdk spit method
    public static String[] split(final String s, final String delimiter) {
	if (s == null) {
	    return null;
	}
	int delimiterLength;
	final int stringLength = s.length();
	if (delimiter == null || (delimiterLength = delimiter.length()) == 0) {
	    return new String[] { s };
	}

	// a two pass solution is used because a one pass solution would
	// require the possible resizing and copying of memory structures
	// In the worst case it would have to be resized n times with each
	// resize having a O(n) copy leading to an O(n^2) algorithm.

	int count;
	int start;
	int end;

	// Scan s and count the tokens.
	count = 0;
	start = 0;
	while ((end = s.indexOf(delimiter, start)) != -1) {
	    count++;
	    start = end + delimiterLength;
	}
	count++;

	// allocate an array to return the tokens,
	// we now know how big it should be
	final String[] result = new String[count];

	// Scan s again, but this time pick out the tokens
	count = 0;
	start = 0;
	while ((end = s.indexOf(delimiter, start)) != -1) {
	    result[count] = s.substring(start, end);
	    count++;
	    start = end + delimiterLength;
	}
	end = stringLength;
	result[count] = s.substring(start, end);

	return result;
    }
}
