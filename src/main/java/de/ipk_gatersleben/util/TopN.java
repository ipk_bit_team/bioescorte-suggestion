/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

// implementation of a Heap data structure, which can support Top N sort with limit Memory
public class TopN<E> {
    private final PriorityQueue<E> p;
    private final int n;

    public TopN(final int n) {
	this.n = n;
	this.p = new PriorityQueue<>(n);
    }

    public void add(final E e) {
	if (this.p.size() < this.n) {
	    this.p.add(e);
	    return;
	}

	@SuppressWarnings("unchecked")
	final Comparable<? super E> head = (Comparable<? super E>) this.p
		.peek();
	if (head.compareTo(e) > 0) {
	    return;
	}
	this.p.poll();
	this.p.add(e);
    }

    public List<E> toList() {
	final List<E> list = new ArrayList<E>();
	while (!this.p.isEmpty()) {
	    final E head = this.p.poll();
	    list.add(head);
	}
	Collections.reverse(list);
	return list;
    }

}
