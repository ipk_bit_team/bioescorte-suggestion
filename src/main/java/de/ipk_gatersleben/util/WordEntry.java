/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;


public class WordEntry implements Comparable<WordEntry> {
    public String name;
    public float similarity;
    
    public WordEntry()
    {
    	
    }

    public WordEntry(String name, float similarity) {
        this.name = name;
        this.similarity = similarity;
    }

    @Override
    public String toString() {
        return this.name + "\t" + similarity;
    }
    
    public int compareTo(WordEntry o) {
        if (this.similarity < o.similarity) {
            return -1;
        } else {
            return 1;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(similarity);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordEntry other = (WordEntry) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(similarity) != Float.floatToIntBits(other.similarity))
			return false;
		return true;
	}

}