/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;


import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

//a memory effect HashMap implementation
public class Counter<K> {
	private TObjectIntMap<K> hm = null;

	public Counter() {
		hm = new TObjectIntHashMap<K>();
	}

	
	public void add(K k) {
		if (hm.containsKey(k)) {
			hm.increment(k);
		} else {
			hm.put(k, 1);
		}
	}
	
	public int size() {
		return hm.size();
	}

	
	public void remove(K k) {
		hm.remove(k);
	}
	
	
	public TObjectIntMap<K> get(){
		return this.hm ;
	}
	
}
