package de.ipk_gatersleben.util;

public class TextContent implements Comparable<TextContent>{
	private int idx;
	private int len;

	public TextContent(int idx, int len) {
		super();
		this.idx = idx;
		this.len = len;
	}
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getLen() {
		return len;
	}
	public void setLen(int len) {
		this.len = len;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idx;
		result = prime * result + len;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextContent other = (TextContent) obj;
		if (idx != other.idx)
			return false;
		if (len != other.len)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextContent [idx=" + idx + ", len=" + len + "]";
	}

	public int compareTo(TextContent textcontent){
		return this.len > textcontent.len ? 1 : this.len < textcontent.len ? -1 : 0;
	}
}
