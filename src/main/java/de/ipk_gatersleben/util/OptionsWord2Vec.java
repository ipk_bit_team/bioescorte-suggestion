/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant
 * Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU General Public License,
 * version 2 which accompanies this distribution, and is available at
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html* (C)
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */
package de.ipk_gatersleben.util;

import uk.co.flamingpenguin.jewel.cli.CommandLineInterface;
import uk.co.flamingpenguin.jewel.cli.Option;

@CommandLineInterface(application = "word2vec")
public interface OptionsWord2Vec {
	@Option(shortName = "i", description = "Please specify the path to train file!")
	String getCorpuspath();
	@Option(shortName = "o", description = "Please specify the path to save the resulting word vectors!")
	String getSaveModelpath();
	@Option(shortName = "f", defaultValue = "200", description = "The feature size of word vectors.")
	int getFeaturesize();
	@Option(shortName = "w", defaultValue = "5", description = "The max skip length between words.")
	int getWindow();
	@Option(shortName = "s", defaultValue = "0.001", description = "Set threshold for occurrence of words. Those that appear with higher frequency in the training data will be randomly down-sampled; default is 1e-3, useful range is (0, 1e-5).")
	double getSample();
	@Option(shortName = "t", defaultValue = "1", description = "The training iterations (default 1).")
	int getIterations();
	@Option(shortName = "a", defaultValue = "0.025", description = "Set the starting learning rate; default is 0.025 for skip-gram.")
	double getAlpha();
	@Option(shortName = "n", defaultValue = "1", description = "The training thread number, default is 1.")
	int getThreadnumber();
	@Option(shortName = "m", defaultValue = "1", description = "Use the continuous bag of words model; default is 1 (use 0 for skip-gram model).")
	int getMethod();
}
